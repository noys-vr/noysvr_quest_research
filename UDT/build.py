import unrealtools
import package
import shutil
import os
import datetime

project_root_path = unrealtools.get_project_root_path()
build_config = unrealtools.get_build_config(project_root_path)
projectfile_name, uproject = unrealtools.get_project_file(project_root_path)

build_succeeded = unrealtools.build_project(project_root_path, projectfile_name, build_config, uproject)
if not build_succeeded:
	raise Exception("Build failed!")

platform_build_path = unrealtools.get_directory_name_by_platform(build_config['build_platform'])
read_path = os.path.join(project_root_path, build_config['build_path'], platform_build_path)

timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
dist_name = '%s_%s_%s' % (projectfile_name, platform_build_path, timestamp)
write_path = r'K:'

print('********** ZIPPING DISTRIBUTION STARTED **********')

packaging_succeeded, package_path  = package.create_zip_file(read_path, write_path, dist_name)
if not packaging_succeeded:
	raise Exception("Packaging falied!")

print('Packaged distribution file %s to %s' % (dist_name, write_path))

print('********** ZIPPING DISTRIBUTION COMPLETED **********')