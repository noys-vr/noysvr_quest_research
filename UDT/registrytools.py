import winreg

def _try_get_key(rawkey, index):
	try:
		value = winreg.EnumKey(rawkey, index)
		return True, value
	except OSError:
		return False, None

def _try_get_value(rawkey, index):
	try:
		value = winreg.EnumValue(rawkey, index)
		return True, value
	except OSError:
		return False, None

def _iterate_raw_key(rawkey, retriever):
	index = 0
	while True:
		keep_iterating, value = retriever(rawkey, index)
		if False == keep_iterating:
			return None
		yield value
		index = index + 1

def iterate_keys(rawkey):
	return _iterate_raw_key(rawkey, _try_get_key)

def iterate_values(rawkey):
	return _iterate_raw_key(rawkey, _try_get_value)

def search_local_key64(path):
	domain = winreg.HKEY_LOCAL_MACHINE
	access = winreg.KEY_READ | winreg.KEY_WOW64_64KEY
	try:
		return winreg.OpenKey(domain, path, 0, access)
	except:
		return None