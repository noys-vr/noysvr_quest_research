import os
import runtools
import registrytools
import json

CONFIGPATH = 'UDT'

class MissingDependencyException(Exception):
	def __init__(self, message):
		super().__init__(message)

class RegistryMisconfiguredException(Exception):
	def __init__(self, message=''):
		super().__init__(message)

def get_installation_path(version):
	search_path = r'SOFTWARE\EpicGames\Unreal Engine\%s' % version

	rawkey = registrytools.search_local_key64(search_path)
	if None == rawkey:
	 	raise MissingDependencyException('No installation of unreal version %s found!' % version)

	for entry in registrytools.iterate_values(rawkey):
		(key, value, _) = entry
		if 'InstalledDirectory' == key:
			return value

	raise RegistryMisconfigured

def get_project_tools_path():
	toolspath, _ = os.path.split(os.path.abspath(__file__))
	return toolspath

def get_project_root_path():
	toolspath = get_project_tools_path()
	return os.path.normpath(os.path.join(toolspath, '..'))	

def get_project_file(project_path):
	for file in os.listdir(project_path):
		if file.endswith('.uproject'):
			projectfile_name, _ = file.split('.')
			with open(os.path.join(project_path, file), 'r') as project_file:
				return projectfile_name, json.load(project_file)

	raise Exception('No uproject file found at %s' % project_path)

def get_build_config(project_path):
	with open(os.path.join(project_path, CONFIGPATH, 'config.json'), 'r') as config_file:
		build_config = json.load(config_file)
		return build_config

	raise Exception('No config file found at %s' % project_path)	

def get_directory_name_by_platform(platform):
	if platform in ('Win64', 'Win32'):
		return 'WindowsNoEditor'

	raise Exception('Missing platform defintion for %s!' % platform)

def start_editor_game_instance(project_path, projectfile_name, uproject, window_config):
	install_directory = get_installation_path(uproject['EngineAssociation'])
	binaries = os.path.join(install_directory, r'Engine\Binaries')
	editor = os.path.join(binaries, r'Win64\UE4Editor.exe')	
	projectfile_path = os.path.join(project_path, '%s.uproject' % projectfile_name)

	params = [
		editor,
		projectfile_path,
		'-game',
		'-ResX=%s' % window_config['width'],
		'-ResY=%s' % window_config['height'],
		'-WinX=%s' % window_config['x'],
		'-WinY=%s' % window_config['y'],
		'-log',
		'-nosteam',
		'-windowed',
	]

	return runtools.run(params, detached=True)

#def start_build_instance(build_path, )

def build_project(project_path, projectfile_name, build_config, uproject):
	project_dir = project_path
	output_directory_name = build_config['build_path']
	build_mode = build_config['build_mode']
	build_platform = build_config['build_platform']
	engine_version = uproject['EngineAssociation']

	projectfile_path = os.path.join(project_dir, '%s.uproject' % projectfile_name)

	install_directory = get_installation_path(engine_version)

	output_path = os.path.join(project_dir, output_directory_name)
	if not os.path.isdir(output_path):
		os.makedirs(output_path)

	binaries = os.path.join(install_directory, r'Engine\Binaries')
	editor = r'Win64\UE4Editor.exe'
	automation_tool = r'DotNET\AutomationTool.exe'

	params = [
		os.path.join(binaries, automation_tool),
		'-ScriptsForProject=%s' % projectfile_path,
		'BuildCookRun',
		'-nocompile',
		'-nocompileeditor',
		'-installed',
		'-nop4',
		'-project=%s' % projectfile_path,
		'-cook',
		'-stage',
		'-archive',
		'-archivedirectory=%s' % output_path,
		'-package',
		'-clientconfig=%s' % build_mode,
		'-ue4exe=UE4Editor-Cmd.exe',
		'-pak',
		'-prereqs',
		'-nodebuginfo',
		'-targetplatform=%s' % build_platform,
		'-build',
		'-utf8output'
	]

	return 0 == runtools.run(params)