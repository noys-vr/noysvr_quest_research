import subprocess

def run(params, detached=False):
	if detached:
		CREATE_NEW_PROCESS_GROUP = 0x00000200
		DETACHED_PROCESS = 0x00000008
		options = DETACHED_PROCESS | CREATE_NEW_PROCESS_GROUP
		subprocess.Popen(params, bufsize=0, creationflags=options)		
		return 0
	else:
		p = subprocess.Popen(params, shell=True, bufsize=0)
		p.wait()
		return p.returncode