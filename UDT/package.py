import subprocess
import os
import registrytools
import unrealtools
import runtools

def get_installation_path():
	search_path = r'SOFTWARE\7-Zip'

	rawkey = registrytools.search_local_key64(search_path)
	if None == rawkey:
	 	raise MissingDependencyException('No installation of 7-Zip found!')

	for entry in registrytools.iterate_values(rawkey):
		(key, value, _) = entry
		if 'Path' == key:
			return os.path.normpath(value)

	raise RegistryMisconfigured

def create_zip_file(read_path, write_path, archive_name, compression = 1):
	read_from = os.path.join(read_path, '*')
	zipname = '%s.zip' % archive_name
	ouput_file_path = os.path.join(write_path, zipname)

	installation_path = get_installation_path()
	toolpath = os.path.join(installation_path, '7z.exe')

	params = []
	params.append(toolpath)
	params.append('a')
	params.append('-bsp1')
	params.append('-tzip')
	params.append('-r')
	params.append('-mx%i' % compression)
	params.append(ouput_file_path)
	params.append(read_from)

	packaging_succeeded = 0 == runtools.run(params)
	return packaging_succeeded, ouput_file_path