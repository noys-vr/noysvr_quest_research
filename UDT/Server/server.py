from flask import Flask
from flask import request as GloablFlaskRequest
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)

class Storage():
	def __init__(self):
		self.active_session = {}
		self.token_counter = 1337
	def update(self, session):
		self.active_session = session
	def get(self):
		return self.active_session
	def isvalid(self):
		return 0 < len(self.active_session)
	def next_token(self):
		token = self.token_counter
		self.token_counter = self.token_counter + 1
		return token

storage = Storage()

def parse_session_request(parser):
	parser.add_argument('sessionid')
	parser.add_argument('ip')
	parser.add_argument('owninguserid')
	parser.add_argument('owningusername')
	parser.add_argument('builduniqueid')
	parser.add_argument('slots')
	parser.add_argument('lan')
	parser.add_argument('port')
	return parser.parse_args()

def add_session(args, external_ip):
	session = {
		'sessionid': args['sessionid'],
		'ip': external_ip,
		'owninguserid': args['owninguserid'],
		'owningusername': args['owningusername'],
		'builduniqueid': args['builduniqueid'],
		'slots': args['slots'],
		'lan': args['lan'],
		'port': args['port'],
	}
	storage.update(session)
	return storage.get()


class Server(Resource):
	def post(self):
		parser = reqparse.RequestParser()
		args = parse_session_request(parser)
		external_ip = GloablFlaskRequest.remote_addr
		add_session(args, GloablFlaskRequest.remote_addr)
		response_data = {
			'external_ip': external_ip,
			'session_token': storage.next_token(),
			'routes': GloablFlaskRequest.access_route
		}
		return response_data, 201

class Client(Resource):
	def get(self):
		if not storage.isvalid():
			return 'No session registered.', 400        
		return storage.get(), 200
	  
api.add_resource(Server, '/host')
api.add_resource(Client, '/join')

app.run(debug=True, host='0.0.0.0')