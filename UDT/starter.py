import unrealtools
import sys
import getopt

def starter(opts):
	window_config = {}
	for opt, arg in opts:
		key = opt[2:]
		window_config[key] = arg
	project_root_path = unrealtools.get_project_root_path()
	projectfile_name, uproject = unrealtools.get_project_file(project_root_path)
	return unrealtools.start_editor_game_instance(project_root_path, projectfile_name, uproject, window_config)

def main(execution_environment, arguments):
	try:
		opts, _args = getopt.getopt(arguments, 'h', ['x=','y=','width=','height='])
		return starter(opts)
	except getopt.GetoptError:
		print(e)
		print('usage: start.py --x 0 --y 0 --width 800 --height 600')
		return 1

if __name__ == '__main__':
	execution_environment, *arguments = sys.argv[0:]
	return_code = main(execution_environment, arguments)
	sys.exit(return_code)